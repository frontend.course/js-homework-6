class NewUser {
    _birthday

    constructor(firstName, lastName, birthday) {
        this._firstName = firstName;
        this._lastName = lastName
    }

    login = function () {
        const firstChar = this._firstName.at(0).toLowerCase();
        return firstChar + this._lastName.toLowerCase()
    }

    get firstName() {
        return this._firstName
    }

    set firstName(value) {
        if (typeof value !== "string" || value.length === 0) {
            console.warn("You must use not empty string for `firstName` field")
            return
        }
        this._firstName = value
    }

    get lastName() {
        return this._lastName
    }

    set lastName(value) {
        if (typeof value !== "string" || value.length === 0) {
            console.warn("You must use not empty string for `lastName` field")
            return
        }
        this._lastName = value
    }

    set birthday(value) {
        if (typeof value !== "string" || value.length === 0) {
            console.warn("You must use not empty string for `birthday` field")
            return
        }

        const regexp = /^\d{2}\.\d{2}\.\d{4}$/
        if (!regexp.test(value)) {
            console.warn("You must use string with format dd.mm.yyyy for `birthday` field")
            return
        }

        const [day, month, year] = value.split(".")
        this._birthday = new Date(+year, month - 1, +day)
    }

    getAge() {
        const today = new Date()

        let age = today.getFullYear() - this._birthday.getFullYear()
        const targetMonth = today.getMonth() - this._birthday.getMonth()
        if (targetMonth < 0 || (targetMonth === 0 && today.getDate() < this._birthday.getDate())) {
            age--
        }
        return age
    }

    getPassword() {
        return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear()
    }
}

function createNewUser(firstName, lastName, birthday) {
    const newUser = new NewUser(firstName, lastName)
    newUser.birthday = birthday
    return newUser
}

let firstName = prompt("Enter first name")
let lastName = prompt("Enter last name")

while (firstName.length === 0 || lastName.length === 0) {
    alert("You must enter not empty string")
    firstName = prompt("Enter first name")
    lastName = prompt("Enter last name")

}

let birthday
while (true) {
    birthday = prompt("Enter birthday in format dd.mm.yyyy")

    const regexp = /^\d{2}\.\d{2}\.\d{4}$/
    if (!regexp.test(birthday)) {
        console.warn("You must use string with format dd.mm.yyyy for `birthday` field")
        continue
    }

    break
}

const user = createNewUser(firstName, lastName, birthday)

console.log(user)
console.log(user.getAge());
console.log(user.getPassword());
